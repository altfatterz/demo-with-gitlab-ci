package com.example;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class DemoWithGitLabCIApp {

	public static void main(String[] args) {
		SpringApplication.run(DemoWithGitLabCIApp.class, args);
	}
}

@Data
@AllArgsConstructor
class Greeting {

	String name;
}

@RestController
class GreetingController {

	@GetMapping
	Greeting greet(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting("Hello " + name);
	}


}